import { defineStore } from "pinia";
import { computed, ref } from "vue";

/**
 * 定义仓库 defineStore
 * xxx指的是文件名 唯一标识
 * export const useXXXStore = defineStore('唯一标识就是文件名xxx', () => {
 * 
 * })
 */
export const useCounterStore = defineStore('counter', () => {
    // setup函数
    /**
     * vuex      pinia
     * state     ref或者reactive
     * mutaions  普通函数
     * actions   普通函数
     * getters   computed
     */
    const count = ref(0)
    const add = () => {
        count.value++
    }
    const asyncAdd = () => {
        setTimeout(() => {
            count.value += 2
        }, 2000)
    }
    const doubleCount = computed(() => count.value * 2)
    return {
        count,
        add,
        asyncAdd,
        doubleCount
    }
})