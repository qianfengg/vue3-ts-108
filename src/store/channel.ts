/**
 * 1. 导入defineStore
 * 2. export const useXXXStore = defineStore('xxx', () => {
 *  // vue3语法
 *      
 * })
 * 3. 提供数据 高亮频道id数据
 * 4. 提供修改数据的方法 改高亮id的方法
 * 5. 把数据和方法在setup函数中return
 */
import { defineStore } from 'pinia';
import { ref } from 'vue';
export const useChannelStore = defineStore('channel', () => {
    const activeId = ref(0)
    const changeActiveId = (id: number) => {
        activeId.value = id
    }
    return {
        activeId,
        changeActiveId
    }
})