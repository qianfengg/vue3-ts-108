/**  
 * 泛型别名 练习 模拟发送请求后，拿到后端接口的数据
 * 语法 类型别名后加尖括号 传入你想要传入的类型，作为参数
 *  type xxx<类型参数A = 可以设置默认值, 类型参数B ...> = {
 *      字段A: 类型参数A
 *      。。。。
 *  }
 * 
 * {
 *  code: 200,
 *  msg: 'xxx',
 *  data: 数据
 * }
 * 
 * 在我们没学泛型前 会用交叉类型做复用，但在这样的例子中不太合适
 *     type UserData = CodeAndMsg & { data: UserList }
 *     type ArticelData = CodeAndMsg & { data: ArticleDetail }
 * 用交叉类型还是有很多重复的代码，需要用泛型优化
 *      type UserData = ApiData<UserList>
 *      type ArticelData = ApiData<ArticleDetail>
 */
{
    // 一般只有一个类型参数，规范上起名会写大写的T 意思就是Type
    type ApiData<T> = {
        code: number
        msg: string;
        data: T
    }
    type User = {
        name: string
        age: number
    }
    // number[] string[] User[]
    type UserList = User[]
    type UserData = ApiData<UserList>
    const userData: UserData = {
        code: 200,
        msg: '获取成功',
        data: [
            {
                name: 'zs',
                age: 18
            }
        ]
    }
    userData
    type ArticleDetail = {
        id: string
        content: string
    }
    /**
     * {
     *  code: 200,
     *  msg: '获取详情成功'
     *  data: {
     *      id: '',
     *      content: ''
     *  }
     * }
     */
    type ArticelData = ApiData<ArticleDetail>
    const ArticleData: ArticelData = {
        code: 200,
        msg: '获取成功',
        data: {
            id: '1',
            content: '文章'
        }
    }
    ArticleData
}
