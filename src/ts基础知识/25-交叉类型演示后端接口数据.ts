/**  
 * 泛型别名 练习 模拟发送请求后，拿到后端接口的数据
 * 语法 类型别名后加尖括号 传入你想要传入的类型，作为参数
 *  type xxx<类型参数A = 可以设置默认值, 类型参数B ...> = {
 *      字段A: 类型参数A
 *      。。。。
 *  }
 * 
 * {
 *  code: 200,
 *  msg: 'xxx',
 *  data: 数据
 * }
 * 
 * 在我们没学泛型前 会用交叉类型做复用，但在这样的例子中不太合适
 *     type UserData = CodeAndMsg & { data: UserList }
 *     type ArticelData = CodeAndMsg & { data: ArticleDetail }
 * 用交叉类型还是有很多重复的代码
 */
{
    // 交叉类型演示 开始
    type CodeAndMsg = {
        code: number
        msg: string;
    }
    type User = {
        name: string
        age: number
    }
    // number[] string[] User[]
    type UserList = User[]
    type UserData = CodeAndMsg & { data: UserList }
    const userData: UserData = {
        code: 200,
        msg: '获取成功',
        data: [
            {
                name: 'zs',
                age: 18
            }
        ]
    }
    userData
    type ArticleDetail = {
        id: string
        content: string
    }
    /**
     * {
     *  code: 200,
     *  msg: '获取详情成功'
     *  data: {
     *      id: '',
     *      content: ''
     *  }
     * }
     */
    type ArticelData = CodeAndMsg & { data: ArticleDetail }
    const ArticleData: ArticelData = {
        code: 200,
        msg: '获取成功',
        data: {
            id: '1',
            content: '文章'
        }
    }
    ArticleData

    // 交叉类型演示结束
}
