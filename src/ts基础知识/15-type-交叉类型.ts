/**
 * type交叉类型 
 * interface extends 接口的继承 为了复用
 * type 复用的语法不是extends 他是用交叉类型 复用 相当于接口的继承
 * 语法 类型A & 类型B (一个与的符号)
 */
{
    type Point2D = {
        x: number,
        y: number
    }
    const point2d: Point2D = {
        x: 1,
        y: 2
    }
    point2d
    // 类型别名 A & 类型别名 B
    // 返回的类型就是A和B的属性都有 - 在这个例子中 xyz三个属性都有了
    type Point3D = Point2D & {  
        z: number
    }
    const point3d: Point3D = {
        x: 1,
        y: 2,
        z: 3,
    }
    point3d
}

