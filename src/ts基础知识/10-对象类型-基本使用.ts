/**
 * 对象类型-基本使用
 */
{
    // 空对象 :{}
    const obj1: {} = {}
    obj1

    // 对象里有属性 换行的写法
    const obj2: {
        name: string,
        age: number
    } = {
        name: 'zs',
        age: 18
    }
    obj2

    // 对象里有属性 不换行写在一行里，需要用分号或者逗号隔开
    const obj3: {a: number ;b: boolean} = {
        a: 1,
        b: true
    }
    obj3

    // type 类型别名 = 类型
    type MyObj = {
        c: null
        d: undefined
        sayHi(): void
        code(skill: string): string
    }
    // 对象中方法的语法 方法名(参数1: 参数类型...): 返回值类型
    const obj4: MyObj = {
        c: null,
        d: undefined,
        sayHi() {
            console.log('1');
        },
        code (skill: string) {
            console.log(skill);
            return skill
        }
    }
    obj4
}
