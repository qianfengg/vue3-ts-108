/**
 * 对象类型-综合练习
 * 
 */
{
    type Student = {
        name: string;
        gender: string;
        score: number;
        height?: number
        study(): void // 学习没有参数 没有返回 无尽的学习
        // 游戏名 string 返回数字 箭头函数类型练习
        play?: (game: string) => number
    }

    const student: Student = {
        name: 'zs',
        gender: 'M',
        score: 100,
        study() {

        },
        play(game) {
            console.log(game);
            return 1
        }
    }
    // 补充 因为play方法是可选的（可以写可以不写）
    // 注意 可选的方法，即使你写了 ts也会给你提示 可能未定义
    // 如何解决 3种方案 第1种 可选链 第二种 加判断（类型守卫）第三种非空断言

    //    student.play?.('王者荣耀')

    // 类型守卫
    //    if (student.play) {
    //     student.play('游戏A')
    //    }
    student.play && student.play('111')

    student.play!('11')

}
