/**
 * interface 继承
 * 继承关键字 extends
 * interface A extends B(接口) 
 * A里面有B的属性和方法
 */
{
    // interface 接口名 {
    //  属性：类型
    //}
    interface Point2D {
        x: number,
        y: number
    }
    const point2d: Point2D = {
        x: 1,
        y: 2
    }
    point2d
    // Point3D 继承了 Point2D的属性和方法
    // Point3D 他有自己的z属性 同时还有了Point2D里面的属性方法
    interface Point3D extends Point2D {
        z: number
    }
    const point3d: Point3D = {
        x: 1,
        y: 2,
        z: 3,
    }
    point3d
}
