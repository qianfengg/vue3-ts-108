/**
 * 函数类型-void类型
 * function fn(): void
 * 函数里面啥都不返回，ts帮你推断 说返回值就是void 他指的就是没有返回任何东西
 * 在ts最新5.1版本，返回值类型配置是undefined 可以不写return undefined
 * 其他低版本 返回值undefined必须要写return undefined 
 * 低版本中返回值void和undefined是有区别的
 * 建议： 返回值类型是undefined 就方法return undefined 因为不管高低版本都可以
 */
{
  function fn (): undefined {
    console.log('say hi');
    return undefined
  }
  console.log(fn());  
}
