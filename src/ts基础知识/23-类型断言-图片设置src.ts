/**  
 * 类型断言 - img元素练习
 * 需求 获取img元素 给src赋值一个网图的链接
 * 类型守卫 - 在这个例子目的判断元素是存在在处理逻辑
 */
{
    // 以下两行代码错误演示
    // const imgEl = document.querySelector('#img') as HTMLImageElement
    // imgEl.src = 'https://img1.baidu.com/it/u=1792397334,3889629355&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=666'
    const imgEl = document.querySelector('#img') as HTMLImageElement | null 
    if (imgEl) {
        imgEl.src = 'https://img1.baidu.com/it/u=1792397334,3889629355&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=666'
    }
}
