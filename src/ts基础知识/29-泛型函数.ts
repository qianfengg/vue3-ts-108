/**  
 * 泛型函数
 * 语法 在执行的括号前面加尖括号 - 箭头函数和普通函数都适用
 * 
 */
{
    function fn<T>(a: T, b: T) {
        console.log(a, b);
    }
    function fn1(a: number, b: number) {
        console.log(a, b);
    }
    fn1(1, 2)
    function fn2(a: string, b: string) {
        console.log(a, b);
    }
    fn2('1', '2')

    // 隐式 通过类型推导
    // 如果类型推导，推导出来的就是你想要的类型，你尖括号泛型可以省略
    fn('1', '2')
    // 显式 自己指定类型
    fn<boolean>(true, false)

    const foo = <A, B>(a: A, b: B) => {
        console.log(a, b);
    }
    foo<number, undefined>(1, undefined)
}
