/**
 * interface 基本使用
 * es6类的语法 class Person {}
 * interface语法类比 描述对象 语法没有等号，类型别名语法才有等号
 * 接口，不要把所有的属性写在一行 规范一行一个属性
 * interface 接口名(大驼峰) {
 *  属性名： 类型
 *  属性方法名： 类型
 * }
 * 使用方式，就是和type的类型注解一样
 * :接口名
 */
{
    // 写个接口Student name age? study方法 play方法？
    interface Student {
        name: string;
        age?: number;
        study(): void;
        play?: (game: string) => boolean;
    }
    let student: Student = {
        name: 'ls',
        study () {

        },
    }
    student
}
