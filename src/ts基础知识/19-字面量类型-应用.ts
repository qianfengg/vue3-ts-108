/**  
 * 字面量类型应用   
 * 一般用于联合类型配合字面量类型，能让我们类型更加的精准
 */
{
    // 比如 性别 如果用字符串类型 是不是范围太广不太符合逻辑 只有可能是男或者女
//   let gender: string = '13241234'  
    let gender: '男' | '女' = '男'
    gender

    type MyDir = 'up' | 'down' | 'left' | 'right';

    // 方法，方法传入的参数是方向 up  down left right
    function changeDirection (dir: MyDir) {
        console.log(dir);
    }
    // changeDirection('down')
    changeDirection('down')
}
 