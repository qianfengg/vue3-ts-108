/**  
 * 类型断言
 * 
 * 有时候你比ts更清楚类型，那你可以使用类型断言
 * 他的语法 as 类型(你确定的类型)
 * 
 * 在我们下面这个例子中，因为我们能确定页面中是有这样一个元素，不可能为null
 * 断言必须是你确定有才能去断言 一定是你确定没问题的
 * 
 * 元素的类型确定类型有2种方案
 * 第一种 打印去控制台看
 * 第二种(推荐) 你创建个你想要的元素，摸一摸然后在把代码删除
 */
{
    // Element | null
    // ts他并不清楚你是个a标签元素，他就只知道你获取的是个元素

    const link = document.querySelector('#link') as HTMLAnchorElement
    console.dir(link);
    link.href
}
