/**
 * 类型注解 （注释解释）
 * 学ts小技巧，先写js，之后去摸鼠标摸一摸变量
 * 语法
 * let age: number = 18
 * 变量：类型 = 值
 * :类型  类型注解
 * 作用
 * 有类型约束 - 给变量什么类型，后面赋值操作都要是这个类型，否则会报错
 * 代码提示会更清晰
 */
{
    let age: number = 18;
    age = 19
    // age = '12'
    // age = true
    let str: string = 'abc'
    str.includes('a')
}
