/**
 * 原始类型
 * number string boolean undefined null
 * undefined和null 用鼠标摸上去类型注解是any 比较特殊，因为我们一般用于初始值
 * js什么类型，类型注解就对应的写
 */
{
   let num: number = 1
   num
   let str: string = '2'
   str
   let flag: boolean = true
   flag
   let u: undefined = undefined
   u
   let n: null = null 
   n
}
