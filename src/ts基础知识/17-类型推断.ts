/**      
 * 类型推断(你不用写类型，ts自己能帮你推断出来什么类型就是昨天鼠标摸一摸的操作)      
 * 1. 变量的初始化
 * 2. 函数的返回值
 */
{
   let age = 18 
   age
   let obj = {a: 1, b: '2'}
   obj

   function add (a: number, b: number) {
    return a + b
   }
   add(1,2)
}
