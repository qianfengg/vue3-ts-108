/**
 * 综合练习
 * 刷新页面后，展示访问历史记录，记录包含：次数和时间。
 * 
 * 1. 时间戳 => 时分秒 
 *      date对象上才有对应获取时分秒的方法
 *         时 getHours
 *         分 getMinutes
 *         秒 getSeconds
 * 
 *     formtTime() 可以不传参数 不传参数直接获取当前时间 转换时分秒
 *     formatTime('2023-07-07 01:00:00') 可以传时间格式的字符串 转换时分秒
 *      formatTime(new Date()) 可以传date类型数据，转换时分秒
 *      
 *      时分秒如果只有一位的 需要前面补0 01:00:00
 *      字符串方法 padStart(最大长度， ’补的字符串‘)
 *      '1'.padStart(2, '0') 往前补， 一共补2位， 用0补
 * 2. 定义次数和时间 数据格式
 *      是个对象数组
 *      对象里有2个属性 次数 - number 时间 - string
 * 3. 数据持久化 localStorage
 *      定义个变量 key 
 *      getItem 取数据
 *      setItem 存数据
 *      先实现取数据 为什么？
 *       1. 添加数据要先获取老的数据（数组）
 *        2.  然后在通过push方法添加最新的数据
 *          3.存到本地
 *  4. 渲染
 *      1. 获取本地的数据
 *      2. 数据渲染到页面        
 */
{
    const formatTime = (date?: string | Date) => {
        // 可能没有 当前时间
        if (!date) date = new Date()
        // 字符串 => 时间date对象
        if (typeof date === 'string') date = new Date(date)
        // 代码走到这里的时候 date必然是个Date类型 
        // date对象才能调用以下3个方法转成时分秒
        const h = date.getHours().toString().padStart(2, '0')
        const m = date.getMinutes().toString().padStart(2, '0')
        const s = date.getSeconds().toString().padStart(2, '0')
        // xx:xx:xx
        return `${h}:${m}:${s}`
    }
    // console.log(formatTime());
    // console.log(formatTime('2023-07-07 01:00:00'));
    // console.log(formatTime(new Date()));

    // 对象数组里的每一项 就是一个Time对象
    type Time = {
        count: number
        time: string
    }

    // 页面展示的列表数据类型
    type List = Array<Time>

    const key = 'vue3-ts-108-time-record'

    // 取数据 - 数组 即使没有数据也希望返回空数组
    function getData () {
        const str = localStorage.getItem(key)
        // 因为我们确定返回的数据就是个对象Time数组 所以可以用类型断言，返回值就是List数据 
        return JSON.parse(str || '[]') as List
    }

    // 存数据
    function setData () {
        // 1. 获取老的数据 要么是有数据的数组 要么是空数组
        const list = getData()
        
        // 2. push新的数据 Time数据 次数 时间 添加好新的数据
        list.push({
            // 假设是空数组 添加数据 次数1， length原本是0 length加1即可
            // 假设里面是有数据的数组 举例 2条数据 新加的数据 次数3 也是length+1
            // count的逻辑 就是length + 1
            count: list.length + 1,
            time: formatTime()
        })
        // 3. 存到本地
        localStorage.setItem(key, JSON.stringify(list))
    }
    // setData()
    function render () {
        // 每次一刷新 调用render 要记录新的数据
        setData()
        const list = getData()
        // 获取div#app的元素
        const el = document.querySelector('#app') as HTMLDivElement
        // console.log(list);
        // 元素里渲染数据 - innerHTML
        // 对象数组转成字符串
        el.innerHTML = list.map(item => `次数：${item.count} 时间: ${item.time}`).join('<br/>')
    }
    render()
}


