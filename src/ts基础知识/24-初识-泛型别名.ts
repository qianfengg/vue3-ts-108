/**  
 * 泛型
 *  类型传参
 *  类比 函数 函数是不是可以传参数
 *      类型 也可以传参数
 * 泛型别名 
 * 语法 尖括号 传入你想要传入的类型，作为参数
 *  type xxx<类型参数A = 可以设置默认值, 类型参数B ...> = {
 *      字段A: 类型参数A
 *      。。。。
 *  }
 */
{
    type MyObj<A = number, B = string> = {
        a: A
        b: B
    }
    // type MyObj1 = {
    //     a: number;
    //     b: string;
    // }
    // type MyObj2 = {
    //     a: boolean;
    //     b: null;
    // }
    const obj1: MyObj<{}, Date> = {
        a: {},
        b: new Date()
    }
    obj1
    const obj2: MyObj<boolean, undefined> = {
        a: true,
        b: undefined
    }
    obj2
}
