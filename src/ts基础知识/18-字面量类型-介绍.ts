/**  
 * 字面量类型介绍    
 */
{
   let age: number = 18
   age = 28
//    age = '1'
    let num: 18 = 18
    // num = 19
    num
    let name: 'jack' = 'jack'
    name
    // name = 'tom'
    let arr: ['a'] = ['a']
    // arr = ['b']
    arr

    let age2 = 18
    age2 = 20
    // age2 = '1'
    const age3 = 18
    // age3 = 20
    age3
}
