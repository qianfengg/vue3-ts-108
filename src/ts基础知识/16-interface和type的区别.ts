/**
 * interface vs type
 *                          interface          type
 * 支持对象类型(相同)           支持                支持
 * 是否可以复用(相同)           可以                可以
 * 支持其他类型(不同)           不支持              支持
 * 复用的语法(不同)             extends            &(交叉类型)
 * 重复声明(不同)              可以，会合并属性      不可以，会报错
 */
{
    // type MyNumberString = number | string
    // type MyNumber = number

    // 重复声明的问题 有的公司规范喜欢第一个字母写个大写I - 意思就是interface
    // 接口重复声明 属性会合并
    interface IMyObj {
        name: string
    }
    interface IMyObj {
        // name: boolean
        age: number
    }
    const myObjInterface: IMyObj = {
        name: 'zs',
        age: 18
    }
    myObjInterface

    // 类型别名重复声明 - 重复声明直接报错,以下代码会报错
    // type TMyObj = {
    //     name: string
    // }
    // type TMyObj = {
    //     age: number
    // }

}

