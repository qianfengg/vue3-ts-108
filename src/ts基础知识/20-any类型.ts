/**  
 * any类型 - 逃避ts检查（随便你怎么写不会报错相当于写js） any英文的意思任意类型
 * anyscript不建议 他相当于你就在写js
 * 1. 显式 - 自己写类型注解 :any 
 * 2. 隐式
 *      初始化没有给值的时候，类型推断就是any类型
 *      函数的参数没有给定类型
 */
{
    let obj: any = { a: 1, b: 2 }
    obj = {
        a: 3,
        b: '5'
    }
    obj = 3
    obj = true;
    obj = null
    obj()

    // 初始化没有给值的时候，类型推断就是any类型
    // let a
    // 函数的参数没有给定类型 => any类型
    // function add (a, b) {
    //     return a + b
    // }
}
