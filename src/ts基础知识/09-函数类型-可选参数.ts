/**
 * 函数类型-可选参数
 * 举例说明 slice方法。可以不传参数 也可以传1个 也可以传2个
 * 可选参数语法 在参数名后面加一个? 可选（可传可不传）
 */
{
    function mySlice (a?: number, b?: number) {
        console.log(a, b);
    }
    mySlice(1, 2)
    mySlice(1)
    mySlice()

    // 注意事项： 可选的必须要在必选的后面 必选的必须在前面
    // 下面的函数参数例子是错误示范
    // function mySlice2 (a?: number, b: number) {
    //     console.log(a, b);
    // }
}
