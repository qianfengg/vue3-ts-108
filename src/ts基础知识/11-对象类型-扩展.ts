/**
 * 对象类型-扩展用法
 * 函数使用箭头函数类型，
 *  写法不同，但意思是等价的
 *  属性(参数A: 类型A...): 返回值类型
 *  属性: (参数A: 类型A...) => 返回值类型
 * 属性设置可选，语法 属性名后面加？
 * 使用类型别名
 */
{
    type MyObj = {
        c: number;
        d: string;
        // a(): void;
        a: () => void;
        // b(foo: boolean): boolean; 
        b: (foo: boolean) => boolean;
        e?: null;
        f?: (a: string, b: string) => string
    }
    const obj1: MyObj = {
        c: 1,
        d: '2',
        a() {

        },
        b(foo: boolean) {
            return foo
        },
        // e: null
        f(a, b) {
            return a + b
        }
    } 
    obj1

    type Config = {
        url: string;
        method?: string;
    };

    // axios参数 对象类型 类型注解
    /**
     * axios({
     *  url: '/aaaa',
     *  method: 'xxx' //不写有默认值get
     * })
     */
    function myAxios (config: Config) {
        console.log(config);
    }
    myAxios({
        url: '',
        
    })
}
