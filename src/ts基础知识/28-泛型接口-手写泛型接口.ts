/**  
 * 泛型接口 - 自己写泛型接口
 * 接口名后面加尖括号
 * 语法 interface xxx<T>
 * 
 * 公司量产机器人 分为id是数字型的机器人 id是字符串型的机器人 。。。。
 * sayHi返回值 把id返回
 */
{
    interface Robot<T = number> {
        id: T
        sayHiA(id: T): T
        sayHiB?: (id: T) => T
    }

    const numberRobot: Robot = {
        id: 1,
        sayHiA(id) {
            return id
        }
    }
    numberRobot
    const stringRobot: Robot<string> ={
        id: '1',
        sayHiA(id) {
            return id
        }
    }
    stringRobot
}
