/**
 * 联合类型
 * 语法 类型A | 类型B | ... 是类型A或者是类型B或者是...
 * 注意联合类型 是一根竖线 
 * js或的逻辑两根竖线
 */
{
    let arr: (string | number)[] = [1, '2', 3, '3']
    arr

    let arr2: (boolean | undefined | null)[] = [true, undefined, null]
    arr2

    // 字符串或者是数组类型的数组
    let arr3: string | number[] = [1, 2, 3]
    arr3

    /**
     * 联合类型综合小练习
     * 写延时器 延时器的初始值null
     * 赋值setTimeout
     */
    let timer: null | number = null
    timer = setTimeout(() => {}, 1000)
}
