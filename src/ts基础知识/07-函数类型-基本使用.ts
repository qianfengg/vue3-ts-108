/**
 * 函数类型-基本使用
 * 本质上就是 给参数和返回值 加类型注解
 * 两种写法
 *  分别指定
 *      语法(a: 类型A, b: 类型B, ....):返回值类型
 *  同时指定
 *      用类型别名 函数的语法去处理
 *      type xxx = (a: 类型A, b: 类型B, ...) => 返回值类型
 *      注意 只能用在函数表达式
 * 
 * 函数声明 和 函数表达式
 * function xxx () {}
 * const xxx = () => {}
 */
{
    // 两个数字相加
    function add1(a: number, b: number): number {
        return a + b
    }
    add1(1, 2)
    // add1('1', 2) error
    // 两个字符串拼接
    const add2 = (a: string, b: string): string => a + b 
    add2('1', '2')
    // add2('1', 222)

    // type 类型名 = 类型
    type MyAddFn = (a: number, b: number) => number 
    const add3: MyAddFn = (a, b) => a + b
    add3(1, 2)
    add3(2, 3)

    // function add4(a, b) {
    //     return a + b
    // }
}
