/**
 * 类型别名
 * 是什么
 *  给类型起别名
 * 怎么用
 *  关键字type - 记忆方式 let a = 1, 
 *  let 变量名 = 值
 *  type 类型名字 = 类型（单个类型或者联合等都可以）
 *  类型名字规范用大驼峰 所有首字母都要大写
 *  之后类型别名的使用 和 类型注解的方式一样
 *  :类型别名
 * 为什么要用
 *  为了复用，简化
 */
{
    type MyArr = (string | number)[]
    let arr: MyArr = [1, '2', 3]
    arr
    let arr2: MyArr = [2, '3', 4]
    arr2
    let arr3: MyArr = [3, '4', 5]
    arr3

    type MyNumber = number
    let a: MyNumber = 1
    a
}
