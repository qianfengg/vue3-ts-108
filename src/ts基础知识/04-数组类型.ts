/**
 * 数组类型
 * 2种写法
 * 第一种： 类型加上[] 比如number[], string[]
 * 第二种：Array<类型> 比如Array<number> Array<null>
 * 建议使用第一种
 */
{
 let arr: number[] = [1, 2, 3] 
 arr
 let arr2: boolean[] = [true, false,] 
 console.log(arr2);
 let arr3: Array<undefined> = [undefined, undefined, undefined]
 arr3
}
