/**  
 * 泛型函数 - 补充 不能想当然的乱写泛型
 * 比如 T+T会报错
 * 语法 在执行的括号前面加尖括号 - 箭头函数和普通函数都适用
 */
{
    // function add<T>(a: T, b: T) {
    //     console.log(a, b);
    // }
    function add1(a: number, b: number) {
        return a + b
    }
    add1(1, 2)
    function add2(a: string, b: string) {
        return a + b
    }
    add2('a', 'b')
}
