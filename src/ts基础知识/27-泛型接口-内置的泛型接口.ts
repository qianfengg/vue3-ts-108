/**  
 * 泛型接口 - 了解内置的泛型接口
 * 我们之前已经学过了？interface Array<T>
 * 面试官问你 什么是泛型 回答：比如我先介绍下内置的泛型接口 interface Array<T>
 * 类型传参
 */
{
    // number[]
    let arr: Array<string | number> = ["1, 2, 3"]
    arr.forEach(item => {
        item
    })
    // push方法是干什么用的 在数组后面添加 返回值是什么 添加后的数组长度
    arr.push(1,2,3)
}
