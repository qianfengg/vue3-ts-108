export interface ApiRes<T = any> {
  data: T;
  message: string;
}

export interface ChannelData {
  channels: Channel[];
}

export interface Channel {
  id: number;
  name: string;
}