export interface ArticleData {
  results: Article[];
  pre_timestamp: string;
}

export interface Article {
  /**
   * 文章id
   */
  art_id: string;
  /**
   * 文章标题
   */
  title: string;
  /**
   * 作者id
   */
  aut_id: string;
  /**
   * 评论数
   */
  comm_count: number;
  /**
   * 发布日期
   */
  pubdate: string;
  /**
   * 作者名字
   */
  aut_name: string;
  /**
   * 是否置顶
   */
  is_top: number;
  /**
   * 图片字段
   */
  cover: Cover;
}

export interface Cover {
  /**
   * 类型
   */
  type: number;
  /**
   * 图片url数组
   */
  images: string[];
}