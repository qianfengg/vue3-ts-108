declare const add: (a: number, b: number) => number

// type 和 interface 可以省略 declare
declare type Point = {
    x: number
    y: number
}

declare const point: (p: Point) => void

export {
    add,
    point
}